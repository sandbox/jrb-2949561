The Status Check module can be used as a status check for use with load
balancers and website monitoring systems. It can perform several checks on
the system:

- Read/write to cache
- Read/write/delete a public file
- Read/write/delete a private file
- Read/write/delete a temporary file

An administrative user can configure which of these standard checks are
performed.

If its URL is hit, it will return a 200 HTTP response code and the current
timestamp if all checks pass. It will return a 500 HTTP response code with no
response if a check fails.

Modules can implement their own hook_status_check_status_alter() to perform
custom tests.
