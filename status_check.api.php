<?php

/**
 * @file
 * Documentation for Status Check API.
 */

/**
 * Alter status check result based on custom tests.
 *
 * This function should only set $success to FALSE on a failure. It should not
 * change the value to TRUE because it may override failures from other tests.
 *
 * @param bool $success
 *   Current status of site.
 */
function hook_status_check_status_alter(&$success) {
  if (!file_get_contents('/tmp/my-special-file')) {
    $success = FALSE;
  }
}
