<?php

namespace Drupal\status_check\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Datetime\Time;

/**
 * Class StatusCheckController.
 *
 * @package Drupal\status_check\Controller
 */
class StatusCheckController extends ControllerBase {

  /**
   * Time object.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $dateTime;

  /**
   * Module handler object.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Lifetime of test cache data in seconds.
   */
  const CACHE_LIFETIME = 10;

  /**
   * StatusCheckController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler object.
   * @param \Drupal\Component\Datetime\Time $date_time
   *   Time object.
   */
  public function __construct(ModuleHandlerInterface $module_handler, Time $date_time) {
    $this->dateTime = $date_time;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('datetime.time')
    );
  }

  /**
   * Returns status of website.
   *
   * Note: Routing option sets "no_cache: TRUE" to prevent page from being
   * cached.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   Response object - Timestamp on success, nothing on failure.
   */
  public function check() {

    $success = TRUE;

    // Allow modules to do their own tests.
    $this->moduleHandler->alter('status_check_status', $success);

    $response = new Response();
    if (!$success) {
      $response->setStatusCode(500);
    }
    $output = ($success) ? $this->dateTime->getRequestTime() : '';
    $response->setContent((string) $output);
    return $response;

  }

}
