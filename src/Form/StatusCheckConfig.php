<?php

namespace Drupal\status_check\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class StatusCheckConfig.
 */
class StatusCheckConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_check_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'status_check.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config = $this->config('status_check.settings');

    $form['info'] = [
      '#type' => 'markup',
      '#markup' => '<div>'
      . $this->t('Select the items that Status check module should check.')
      . '</div>',
    ];
    $form['check_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check cache'),
      '#description' => $this->t('Confirm that the system can write and read from cache.'),
      '#default_value' => $config->get('check_cache'),
    ];

    if (!$filesystems = $config->get('check_filesystems')) {
      $filesystems = [];
    }
    $options = [
      'public' => $this->t('Public'),
      'private' => $this->t('Private'),
      'temporary' => $this->t('Temporary'),
    ];
    $form['check_filesystems'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Check filesystems'),
      '#description' => $this->t('Confirm that the system can write, read, and delete from these filesystems.'),
      '#options' => $options,
      '#default_value' => $filesystems,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('status_check.settings')
      ->set('check_cache', $values['check_cache'])
      ->set('check_filesystems', array_filter($values['check_filesystems']))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
